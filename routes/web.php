<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SearchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route Controller
Route::resource('rooms', 'App\Http\Controllers\RoomController');

Route::resource('receipts', 'App\Http\Controllers\ReceiptController');

Route::resource('onboard', 'App\Http\Controllers\OnboardController');

Route::get("/search",[SearchController::class, 'index'])->name('web.index');

@extends('layouts.app')

@section('content')

<h1 class="text-center">Loyverse POS System</h1>

<hr><br>

<div class="row">
    <div class="col-sm-6">
        <h2>Receipt Modal : Pop Out</h2>
    </div>
    <div class="col-sm-6 text-right">
     <!-- Modal Button Receipt Detail-->
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info" id="myBtn">View Receipt Modal</button>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1"  role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <!-- Modal content-->
            <div class="modal-content w-auto">
                <div class="modal-header">
                <div class="d-flex justify-content-start">
                    <label></label>
                </div>
                <div class="d-flex justify-content-between pl-4">
                    <label class="font-weight-bold text-capitalize">Receipt Details</label>
                </div>
                <div class="d-flex justify-content-end pb-2">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                </div>

                <div class="modal-body">
                <!-- Receipt Total -->

                    <div class="flexcolumn text-center font-weight-bold">
                        <h4>$ 14.20</h4>
                        <p>Total</p>
                    </div>

                <!-- End of Receipt Total -->

                <hr>

                <!-- Receipt Detail -->
                <!-- Row 1 -->
                <div class="receiptlabel text-left">
                <label>Order: BizIT Solutions - 11:46</label>

                </div>

                <!-- Row 2 -->
                <div class="receiptlabel text-left">
                    <label>Cashier: Owner</label>
                </div>

                <!-- Row 3 -->
                <div class="receiptlabel text-left">
                    <label>POS: POS 1</label>
                </div>

                <hr>

                <!-- Customer Detail -->
                <!-- Row 1 -->
                <div class="receiptlabel text-left">
                    <label>Customer: BizIT Solutions</label>
                </div>

                <!-- Row 2 -->
                <div class="receiptlabel text-left">
                    <label>Contact: 7225496</label>
                </div>


                <!-- Row 3 -->
                <div class="receiptlabel text-left">
                <label>Email: sale@bizitsolutions.biz</label>
                </div>

                <hr>

                <!-- Room Detail -->
                <!-- Row 1 -->
                <div class="receiptlabel text-left font-weight-bold">
                    <label>Room 2</label>
                </div>

                <hr>

                <!-- Row 2 -->
                <div class="receiptlabel roomtitle text-left">
                    <label>Grand Room</label>
                </div>

                <hr>


                <!-- Order Detail -->
                <!-- Row 1 Left -->
                <div class="receiptlabel d-flex justify-content-between">

                <div class="p-0 d-inline-flex text-left">
                    <label>7-UP</label>
                </div>
                <!-- Row 1 Right -->
                <div class="p-0 align-self-baseline text-right">
                    <label>$ 14.20</label>
                </div>

                </div>


                <!-- Row 1 -->
                <div class="receiptlabel text-left">
                    <label>1 x $ 13.00</label>
                </div>

                <!-- Row 2 -->
                <div class="receiptlabel text-left">
                    <label>+ Add Chips ($ 1.20)</label>
                </div>


                <hr>

                <!-- Account Total -->
                <!-- Row 1 -->
                <div class="receiptlabel text-left font-weight-bold d-flex justify-content-between">
                <div class="p-0">
                <label>Room 2</label>
                </div>
                <div class="p-0">
                <label>$ 14.20</label>
                </div>
                </div>

                <!-- Row 2 -->
                <div class="receiptlabel text-left d-flex justify-content-between">
                <div class="p-0">
                <label>Account</label>
                </div>
                <div class="p-0">
                <label>$ 14.20</label>
                </div>
                </div>

                <hr>

                <!-- Date Receipt Number -->
                <!-- Row 1 -->
                <div class="receiptlabel d-flex justify-content-between ">
                <div class="p-0">
                    <label>Sep 15, 2021. 11:47 AM</label>
                </div>
                <div class="p-0">
                    <label>&#8470;. 1-1009</label>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            </div>

        </div>

    </div>
    <!-- End of Modal Button Receipt Detail -->

    <!-- Modal Button Javascript -->
    <script>
    $(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
    });
    </script>
    </div>
</div>




@endsection

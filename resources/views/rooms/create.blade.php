@extends('layouts.app')

@section('content')

 <h1 class="text-center">Loyverse POS System</h1>

 <hr>

 <div class="row">
     <div class="col-sm-12 text-right">
        <a class="text-right" href="/rooms" class="btn btn-secondary">Go Back To Previous Page</a>
     </div>
 </div>

 <br>

 <div class="row list-group-item">

     <div class="row">
         <div class="col-sm-12">
         <h3>Form : Create New Room</h3>
         </div>
     </div>

     <div class="row">
        <div class="col-sm-12">
        <h6>Please fill in all the Empty Field</h6>
        </div>
    </div>

     <hr>

    <form action="/rooms" method="POST" enctype="multipart/form-data">
    @csrf
        <div class="row">
            <div class="form-group col-sm-8">
                <label for="title">Room Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Grand Room, Big Room, Small Room etc.">
            </div>

            <div class="form-group col-sm-4" id="room_number" name="room_number">
            <label for="title">Room Number</label>
            <select id="room_number" class="form-control" name="room_number">
                    <option>-- Select Room Number --</option>
                    <option value="NONE">NONE</option>
                    <option value="ROOM 1">ROOM 0</option>
                    <option value="ROOM 1">ROOM 1</option>
                    <option value="ROOM 2">ROOM 2</option>
                    <option value="ROOM 3">ROOM 3</option>
                    <option value="ROOM 4">ROOM 4</option>
                    <option value="ROOM 5">ROOM 5</option>
                    <option value="ROOM 6">ROOM 6</option>
                    <option value="ROOM 7">ROOM 7</option>
            </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-8">
                <label for="title">Description</label>
                <input type="text" class="form-control" id="description" name="description" placeholder="Very Big Room, Very Small Room">
            </div>

            <div class="form-group col-sm-4" id="room_type" name="room_type">
            <label for="title">Room Type</label>
            <select id="room_type" class="form-control" name="room_type">
                    <option>-- Select Room Type --</option>
                    <option value="NONE">NONE</option>
                    <option value="FRONT DESK">FRONT DESK</option>
                    <option value="OFFICE">OFFICE</option>
                    <option value="MEETING">MEETING</option>
                    <option value="HALL">HALL</option>
                    <option value="DINING">DINING</option>
                    <option value="STORAGE">STORAGE</option>
                    <option value="KITCHEN">KITCHEN</option>
                    <option value="CHILLER">CHILLER</option>
                    <option value="RESTAURANT">RESTAURANT</option>
                    <option value="BATHROOM">BATHROOM</option>
                    <option value="POOL">POOL</option>
                    <option value="GYM">GYM</option>
                    <option value="PRIVATE">PRIVATE</option>
                    <option value="GUEST">GUEST</option>
                    <option value="STANDARD">STANDARD</option>
                    <option value="EXCLUSIVE">EXCLUSIVE</option>
                    <option value="SUITE">SUITE</option>
                    <option value="APARTMENT">APARTMENT</option>
            </select>
            </div>
        </div>


        <div class="row">
            <div class="form-group col-sm-12">
                <label for="title">Upload Room Image</label>
                <input type="file" id="image_url" name="image_url" class="form-control">
            </div>
        </div>


        <hr>

        <br>

        <div class="row">
        <div class="form-group col-sm-12 text-right">
            <button type="submit" class="btn btn-primary">Add / Submit Room</button>
        </div>
        </div>

    </form>

</div>

@endsection

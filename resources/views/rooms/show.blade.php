@extends('layouts.app')

@section('content')

<h1 class="text-center">Loyverse POS System</h1>

<hr>

<a href="/rooms" class="btn btn-secondary">Go Back To Previous Page</a>

<br>
<br>

<div class="row list-group-item">

    <div class="row">
        <div class="col-sm-12">
            <h2>Detail of the Room</h2>
        </div>
    </div>

    <hr>

    <!-- Room Detail -->
    <div class="row">
        <div class="col-sm-1">
            <label>Id.</label>
        </div>
        <div class="col-sm-1">
            <h4>{{ $room->id }}</h4>
        </div>
        <div class="col-sm-2">
            <label>Name</label>
        </div>
        <div class="col-sm-4">
            <h5>{{ $room->name }}</h5>
        </div>
        <div class="col-sm-2">
            <label>No.</label>
          </div>
          <div class="col-sm-2">
                <p>{{$room->room_number}}</p>
          </div>
    </div>



    <hr>

    <div class="row">
        <div class="col-sm-4">
            <small>Image Filename</small>
        </div>
        <div class="col-sm-8">
            <h5>{{ $room->image_url }}</h5>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-4">
            <small>Image</small>
        </div>
        <div class="col-sm-8">
            <img src="/storage/image_url/{{ $room->image_url }}" class="img-responsive" style="max-height:200px; max-width:300px;" alt="" srcset=""/>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-6">
            <small>Description</small>
             <p>{{ $room->description }}</p>
        </div>
   </div>

   <hr>

    <div class="row">
        <div class="col-sm-12"><br>
            <p class="text-right"><small>{{ $room->name }} Created on {{ $room->created_at }}</small></p>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-3 text-right">
        <!-- Edit Link -->
        <a href="/rooms/{{$room->id}}/edit" class="btn btn-secondary" >Edit Room</a>
        </div>

        <div class="col-sm-3">
        <!-- Delete Function -->
          <form action="/rooms/{{  $room->id  }}" method="POST">
            @method('DELETE')
            @csrf
            <div class="form-group text-right">
                <button type="submit" class="btn btn-danger">Delete Room</button>
            </div>
           </form>
        </div>
    </div>

</div>

 @endsection







@extends('layouts.app')

@section('content')

<h1 class="text-center">Loyverse POS System</h1>

<hr><br>

<div class="row">
    <div class="col-sm-5">
        <h2>Available Room</h2>
    </div>
    <div class="col-sm-7 text-right">
        <a class="btn btn-success" href="/rooms/create">Add New Room</a>
    </div>
</div>

<br>

    <div class="row justify-content-center">
        <div class="col-md-12 table-responsive-sm">
        @if(isset($rooms))
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>No.</th>
                        <th>Type</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($rooms) > 0)
                    @foreach($rooms as $room)
                    <tr>
                        <td>{{ $room->id }}</td>
                        <td><img src="/storage/image_url/{{ $room->image_url }}" class="img-responsive rounded" style="max-height:100px; max-width:100px;" alt="" srcset="" /></td>
                        <td>{{ $room->name }}</td>
                        <td>{{ $room->room_number }}</td>
                        <td>{{ $room->room_type }}</td>
                        <td>{{ $room->created_at }}</td>
                        <td>{{ $room->updated_at }}</td>
                        <td><a href="/rooms/{{ $room->id }}">View {{ $room->name }}</a></td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>No Room Found</td>
                    </tr>
                    @endif
                </tbody>
            </table>
            <div class="pagination-block">
                {{ $rooms->links('include.paginationlinks') }}

                {{-- {{ $rooms->links( "pagination::bootstrap-4") }} --}}
            </div>
        @endif
        </div>
    </div>
</div>

@endsection

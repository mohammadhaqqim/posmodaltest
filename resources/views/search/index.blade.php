@extends('layouts.app')

@section('content')

<h1 class="text-center">Loyverse POS System</h1>

<hr><br>

<div class="row">
    <div class="col-sm-12">
        <h2>Search Room Name : Letter Detect</h2>
    </div>
</div>

<br>

<div class="row">
    <div class="col">
        <form action="{{ route('web.index') }}" method="GET">
         <div class="row form-group">
            <div class="col-sm-10">
              <input type="text" class="form-control" name="query" placeholder="Search Name by letter A - Z">
            </div>
            <div class="col-sm-2">
              <button type="submit" class="btn btn-primary">Search</button>
           </div>
          </div>
     </form>
   </div>
 </div>

 <br>

 <div class="row">
    <div class="col-sm-12">
        <h4>Data Table : List of Room</h4>
    </div>
</div>

<br>

 <div class="row">
        <div class="col-md-12">
           @if(isset($rooms))
            <table class="table table-hover text-center">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>No.</th>
                        <th>Type</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($rooms) > 0)
                    @foreach($rooms as $room)
                    <tr>
                        <td>{{ $room->id }}</td>
                        <td><img src="/storage/image_url/{{ $room->image_url }}" class="img-responsive rounded" style="max-height:100px; max-width:100px;" alt="" srcset="" /></td>
                        <td>{{ $room->name }}</td>
                        <td>{{ $room->room_number }}</td>
                        <td>{{ $room->room_type }}</td>
                        <td>{{ $room->created_at }}</td>
                        <td>{{ $room->updated_at }}</td>
                        <td><a href="/rooms/{{ $room->id }}">View {{ $room->name }}</a></td>
                    </tr>
                       @endforeach
                       @else
                       <tr class="text-center">
                          <td>No Room Found</td>
                       </tr>
                       @endif
                  </tbody>
            </table>

            <div class="pagination-block">
                {{ $rooms->links('include.paginationlinks') }}
            </div>
           @endif
        </div>
    </div>


@endsection

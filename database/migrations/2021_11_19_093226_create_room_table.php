<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->uuid('pos_id')->nullable();
            $table->string('name', 64);
            $table->string('description');
            $table->enum('room_number',['NONE','ROOM 0','ROOM 1', 'ROOM 2', 'ROOM 3', 'ROOM 4', 'ROOM 5', 'ROOM 6', 'ROOM 7'])->default('NONE');
            $table->enum('room_type',['NONE','FRONT DESK', 'OFFICE', 'MEETING', 'HALL', 'DINING', 'STORAGE', 'KITCHEN', 'CHILLER', 'RESTAURANT', 'BATHROOM', 'POOL','GYM', 'PRIVATE', 'GUEST', 'STANDARD', 'EXCLUSIVE', 'SUITE', 'APARTMENT'])->default('NONE');
            $table->text('image_url')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}

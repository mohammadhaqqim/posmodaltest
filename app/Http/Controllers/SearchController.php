<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Room;

class SearchController extends Controller
{
    function index(Request $request){

        // strlen only for accurate name to search

        if(isset($_GET['query']) /* && strlen($_GET['query']) > 1 */){

            $search_text = $_GET['query'];
            $rooms = DB::table('rooms')->where('name', 'LIKE', '%'.$search_text.'%')->paginate(5);
            $rooms->appends($request->all());
            return view('search.index',['rooms'=>$rooms]);

        } else {
             return view('search.index');
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class RoomController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $rooms = room::orderBy('created_at','desc')->paginate(5);

        return view('rooms.index')->with('rooms', $rooms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'room_number' => 'required',
            'room_type' => 'required',
            'image_url' => 'image|nullable|max:1999',
        ]);

            //Handle File Upload
             if($request->hasFile('image_url')){
                //Get Filename with the Extension
                $filenameWithExt = $request->file('image_url')->getClientOriginalName();
                //Get Just Filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                //Get just Ext
                $extension = $request->file('image_url')->getClientOriginalExtension();
                //Filename to Store
                $fileNameToStore = $filename.'_'.time().'.'.$extension;
                //Upload Image
                $path = $request->file('image_url')->storeAs('public/image_url', $fileNameToStore);
            } else {
                $fileNameToStore = 'noimage.jpg';
            }

        // Create Room
        $room = new room;
        $room->pos_id = Str::uuid();
        $room->name = $request->input('name');
        $room->description = $request->input('description');
        $room->room_number = $request->input('room_number');
        $room->room_type = $request->input('room_type');
        $room->image_url = $fileNameToStore;
        $room->save();

        return redirect('/rooms')->with('Success','Room Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $room = room::find($id);
        return view('rooms.show')->with('room', $room);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(room $room)
    {
        return view('rooms.edit')->with('room', $room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'room_number' => 'required',
            'room_type' => 'required',
            'image_url' => 'image|nullable|max:1999'
        ]);

        //Handle File Upload
        if($request->hasFile('image_url')){
            //Get Filename with the Extension
            $filenameWithExt = $request->file('image_url')->getClientOriginalName();
            //Get Just Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just Ext
            $extension = $request->file('image_url')->getClientOriginalExtension();
            //Filename to Store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //Upload Image
            $path = $request->file('image_url')->storeAs('public/image_url', $fileNameToStore);
        }

        // Update Room
        $room = room::find($id);
        $room->pos_id = Str::uuid();
        $room->name = $request->input('name');
        $room->description = $request->input('description');
        $room->room_number = $request->input('room_number');
        $room->room_type = $request->input('room_type');
          if($request->hasFile('image_url')){
          $room->image_url = $fileNameToStore;
        }
        $room->save();

        return redirect('/rooms')->with('Success','Room Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(room $room)
    {
        $room->delete();

       //Delete Image
       if($room->image_url != 'noimage.jpg'){
        Storage::delete('public/image_url/'.$room->image_url);
       }

        return redirect('/rooms')->with('Success', 'Room Removed');
    }

}
